require essioc
require rfcond

# General
epicsEnvSet("M", "RFQ-010:SC-FSM-002")
epicsEnvSet("EVR", "RFQ-010:RFS-EVR-101:")
epicsEnvSet("LLRF", "RFQ-010:RFS-LLRF-101:")
epicsEnvSet("LLRFD", "RFQ-010:RFS-DIG-101:")
epicsEnvSet("FIM", "RFQ-010:RFS-FIM-101:")
epicsEnvSet("RFS", "RFQ-010:RFS-")
#- VACX configuration for RFQ
epicsEnvSet("VAC1", "RFQ-010:Vac-VGC-10000:PrsR CPP")
epicsEnvSet("VAC2", "RFQ-010:Vac-VGC-40000:PrsR CPP")
epicsEnvSet("VAC3", "RFQ-010:Vac-VGC-20000:PrsR CPP")
epicsEnvSet("VAC4", "RFQ-010:Vac-VGC-30000:PrsR CPP")
#- VACIX are same for DTL1 and RFQ
epicsEnvSet("VACI1","RFQ-010:RFS-VacMon-110:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI2","RFQ-010:RFS-VacMon-120:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI3","RFQ-010:RFS-VacMon-130:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI4","RFQ-010:RFS-VacMon-140:Status-Ilck-RB.RVAL CPP")


#Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- Load FIM interlock list
iocshLoad("$(rfcond_DIR)/fim-interlock-list.iocsh", "M=${M}:")

iocshLoad("$(rfcond_DIR)/rfcond.iocsh")

iocInit()
date                                                                                                                                           
